package main

import (
    "fmt"
    "context"
    "os"
    "strconv"
    "github.com/aws/aws-lambda-go/lambda"
    "github.com/aws/aws-lambda-go/events"
    "github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/aws/session"
    "github.com/aws/aws-sdk-go/service/dynamodb"
)

var ddb *dynamodb.DynamoDB
func init() {
    region := os.Getenv("AWS_REGION")
    if session, err := session.NewSession(&aws.Config{
        Region: &region,
    }); err == nil {
        ddb = dynamodb.New(session)
    } else {
        fmt.Println(fmt.Sprintf("Failed to connect to AWS: %s", err.Error()))
    }
}

func CompleteTodo(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
    fmt.Println("CompleteTodo")

    var (
        id = request.PathParameters["id"]
        tableName = aws.String(os.Getenv("TODOS_TABLE_NAME"))
        done, _ = strconv.ParseBool(request.QueryStringParameters["done"])
        doneColumn = "done"
    )

    fmt.Println("done: ", done)

    input := &dynamodb.UpdateItemInput{
        Key: map[string]*dynamodb.AttributeValue{
            "id": {
                S: aws.String(id),
            },
        },
        UpdateExpression: aws.String("set #d = :d"),
        ExpressionAttributeNames: map[string]*string{
            "#d": &doneColumn,
        },
        ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
            ":d": {
                BOOL: aws.Bool(done),
            },
        },
        ReturnValues: aws.String("UPDATED_NEW"),
        TableName: tableName,
    }
    _, err := ddb.UpdateItem(input)

    var agpr events.APIGatewayProxyResponse

    if err == nil {
      agpr.Body = request.Body
      agpr.StatusCode = 200
    } else {
      agpr.Body = err.Error()
      agpr.StatusCode = 500
    }

    return agpr, nil
}

func main() {
  lambda.Start(CompleteTodo)
}
